import { useParams } from "react-router-dom";

const StudentDetailPage = () => {
  let { studentId } = useParams();

  return (
    <div>
      <h1>Student ID:</h1>
      <p>{studentId}</p>
    </div>
  );
};

export default StudentDetailPage;
