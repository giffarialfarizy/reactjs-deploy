import { useNavigate } from "react-router-dom";

const StudentListPage = () => {
  const dummyStudentData = [
    {
      id: "reactjs-001",
      name: "Annisa",
    },
    {
      id: "reactjs-002",
      name: "Bagas",
    },
    {
      id: "reactjs-003",
      name: "Chandra",
    },
    {
      id: "reactjs-004",
      name: "Dessy",
    },
  ];

  const navigate = useNavigate();

  const handleEdit = (event) => {
    const studentId = event.target.value;
    navigate(`${studentId}`);
  };

  return (
    <div>
      <h1>Student List</h1>
      <ul>
        {dummyStudentData &&
          dummyStudentData.map((student) => {
            return (
              <li key={student.id} value={student.id} onClick={handleEdit}>
                {student.id} - {student.name} |&nbsp;
                <button value={student.id} onClick={handleEdit}>
                  Detail
                </button>
              </li>
            );
          })}
      </ul>
    </div>
  );
};

export default StudentListPage;
