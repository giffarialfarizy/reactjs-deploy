import { Link } from "react-router-dom";

const HeaderComponent = () => {
  return (
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/students">Students</Link>
        </li>
      </ul>
    </nav>
  );
};

export default HeaderComponent;
