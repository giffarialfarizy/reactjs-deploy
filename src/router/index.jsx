import { Route, Routes } from "react-router-dom";
import HomePage from "../pages/HomePage";
import StudentListPage from "../pages/StudentListPage";
import StudentDetailPage from "../pages/StudentDetailPage";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/students" element={<StudentListPage />} />
      <Route path="/students/:studentId" element={<StudentDetailPage />} />
    </Routes>
  );
};

export default Router;
