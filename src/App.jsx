import { BrowserRouter } from "react-router-dom";
import Router from "./router";
import HeaderComponent from "./components/HeaderComponent";

function App() {
  return (
    <BrowserRouter>
      <HeaderComponent />
      {/* Page Content Here */}
      <Router />
    </BrowserRouter>
  );
}

export default App;
